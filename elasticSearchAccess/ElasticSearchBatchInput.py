'''
Created on 1 Jul 2018

@author: CargoInShip PS Team
'''

import mysql.connector 
import sshtunnel
import csv
import json
from datetime import datetime, timedelta
from elasticsearch import Elasticsearch

# function to ensure usability of spot data entry 
def is_soe_entry_usable(soe_entry):
    if soe_entry["eta"] is None or soe_entry["abbreviation"] is None:
        return False
    else:
        return True  

if __name__ == '__main__': 

    _host = r'developer.cargoinship.com'
    _ssh_port = 22
    _username = r'munsteruniversity'
    _password = r'huHzJKb459Wz'
 
    _remote_bind_address = r'24v-azu-db001.mysql.database.azure.com'
    _remote_mysql_port = 3306
    _local_bind_address = r'127.0.0.1'
    _local_mysql_port = 3306
 
    _db_user = r'dev_cargoinship_service@24v-azu-db001' #
    _db_password = r'cM2ur5FdIqh6'
    _db_name = r'dev_cargo'
 
# THIS IS ONLY NEEDED WHEN THIS CODE WILL BE EXECUTED OUTSIDE THE VM OF 24VISION
# sets up the connection 
    with sshtunnel.SSHTunnelForwarder(
            (_host, _ssh_port),
            ssh_username=_username,
            ssh_password=_password,
            remote_bind_address=(_remote_bind_address, _remote_mysql_port),
            local_bind_address=(_local_bind_address, _local_mysql_port)
    ) as tunnel:
        connection = mysql.connector.connect(
            user=_db_user,
            password=_db_password,
            host=_local_bind_address,
            database=_db_name,
            ssl_ca=r'../config/BaltimoreCyberTrustRoot.crt.pem')   

        cursor = connection.cursor(buffered=True)         
             
        aisForecastColumnQuery = ("SELECT column_name FROM information_schema.columns WHERE table_name='ais_forecast'")              
        cursor.execute(aisForecastColumnQuery)
        aisForecast_table_column_names = cursor.fetchall()        
        aisForecastQuery = ("SELECT * FROM dev_dbpsbulkcargo.ais_forecast WHERE kibana_extracted IS NULL OR kibana_extracted = 0")
        cursor.execute(aisForecastQuery)
        aisForecast_table_sample_datasets = cursor.fetchall()
           
#       
        shipOfferExtracted_table_column_names = [("ship_offer_extracted_ID",), ("emailID",), ("ship_name",), ("open_date",), ("loading_place",), ("eta",), ("abbreviation",), ("kibana_extracted",)]
        shipOfferExtractedQuery = ("SELECT ship_offer_extracted_ID, emailID, ship_name, open_date, loading_place, standardized_date AS eta, standardized_region AS abbreviation, kibana_extracted FROM dev_dbpsbulkcargo.ship_offer_extracted WHERE kibana_extracted = 0 OR kibana_extracted IS NULL AND emailID >= 1106 ")
        cursor.execute(shipOfferExtractedQuery)
        shipOfferExtracted_table_sample_datasets = cursor.fetchall()
             
        es = Elasticsearch()
        column_value_dict = {} 
# creation of the ship offer and AIS forecast index in case elastic search is running     
#     es.indices.create(index='ship_offer_extracted-index', ignore=400) 
#     es.indices.create(index='forecast-index', ignore=400)  

# for loop in order to add respective AIS forecast entries to ElasticSearch 

        aisDataUpdateCommand = ("UPDATE dev_dbpsbulkcargo.ais_forecast SET kibana_extracted = %s WHERE ais_forecast_id = %s")          
        print( aisForecast_table_column_names)
        for row in aisForecast_table_sample_datasets:
                         
            column_counter = 0 
            for column in aisForecast_table_column_names:             
                column_value_dict[str(column[0])] = row[column_counter]                 
                column_counter+= 1
                   
#         print("anchortime:" + str (column_value_dict.get("anchortime")) )   
#                  
            while column_value_dict.get("anchortime") != -1:
                json_body = json.dumps(column_value_dict, indent=4, sort_keys=True, default=str)
                print(json_body)
    #             es.index(index="forecast-index", doc_type="forecast", body= json_body)
                new_anchortime = column_value_dict.get("anchortime") - 1
                column_value_dict["anchortime"] = new_anchortime 
                        
                new_eta = column_value_dict.get("eta") + timedelta(days=1)
                column_value_dict["eta"] = new_eta
                           
            # set kibana extracted for entry to 1   
            ais_update_val = ("1", column_value_dict.get("ais_forecast_id"))
            cursor.execute(aisDataUpdateCommand, ais_update_val)
            connection.commit()       
            print("Update successful")                          
            column_value_dict.clear()
#       
# for loop in order to add respective SPOT data entries to ElasticSearch     

        spotDataUpdateCommand = ("UPDATE dev_dbpsbulkcargo.ship_offer_extracted SET kibana_extracted = %s WHERE ship_offer_extracted_ID = %s")   
        print(shipOfferExtracted_table_column_names)
        for row in shipOfferExtracted_table_sample_datasets:          
            column_counter = 0 
            for column in shipOfferExtracted_table_column_names:
                     
                if type(row[column_counter]) is bytearray:
                    decoded_value = row[column_counter].decode()
                    column_value_dict[str(column[0])] = decoded_value
                     
                else:
                    column_value_dict[str(column[0])] = row[column_counter] 
                                                 
                column_counter+= 1
               
            if is_soe_entry_usable(column_value_dict) == True:
                print("Useful")
                json_body = json.dumps(column_value_dict, indent=4, sort_keys=False, default=str)
                es.index(index="ship_offer_extracted-index", doc_type="ship_offer_extracted", body= json_body)  
                print(json_body)
            
            # set kibana extracted for entry to 1       
            spot_update_val = ("1", column_value_dict.get("ship_offer_extracted_ID"))
            cursor.execute(spotDataUpdateCommand, spot_update_val)
            connection.commit()
            print("Update successful") 
            column_value_dict.clear()
                      
    print("Success")
    
    
    

        
    
    

    
        
    
    
        
        